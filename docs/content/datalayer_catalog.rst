==================
Datalayer Catalogs
==================

The Datalayer Catalog is a place to organize the many datasets that
are displayed and accessed on the platform.

Impact Observatory uses the open source `stac-fastapi
<https://github.com/stac-utils/stac-fastapi>`_ service manages the
catalog and stores the dataset GeoTIFFs in Azure Blob storage.

The structure of a STAC catalog is hierarchical and flexible. Impact
Observatory has chosen to use the following structure: 

A STAC Catalog - is a simple, flexible JSON file of links that
provides a structured way to organize and browse STAC Items, which are
the individual datalayers. IO uses a single STAC Catalog containing
all STAC collections for scientific datasets. 

STAC Collection - holds additional information such as the extents,
license, keywords, providers, etc that describe all of the STAC Items
that fall within the Collection. IO generally creates a single STAC
collection per science dataset or group of closely related datasets. 

STAC Item - is the core atomic unit, representing a single
spatiotemporal asset as a GeoJSON feature plus datetime and links. IO
has used STAC items to point to the individual tile URLs of a science
dataset stored in Azure. 

Read more about STAC at https://stacspec.org/
